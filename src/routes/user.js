const passport = require("passport");
const {
  loginPage,
  registerPage,
  login,
  loginFailed,
  registerFailed,
  logout,
} = require("../controllers/user");

module.exports = (router) => {
  router.get("/login", loginPage);
  router.post(
    "/login",
    passport.authenticate("login-local", { failureRedirect: "/login-failed" }),
    login
  );
  router.get("/register", registerPage);
  router.post(
    "/register",
    passport.authenticate("register-local", {
      failureRedirect: "/register-failed",
    }),
    login
  );
  router.get("/login-failed", loginFailed);
  router.get("/register-failed", registerFailed);
  router.get("/logout", logout);

  //Facebook

  router.get("/auth/facebook", passport.authenticate("facebook"));

  router.get(
    "/auth/facebook/callback",
    passport.authenticate("facebook", {
      failureRedirect: "/login-failed",
    }),
    login
  );

  return router;
};
