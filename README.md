# coder-desafio-29

Passport-local

## endpoints:

[GET]/api/productos - Lista todos los productos

[GET]/api/productos/:id - Trae un único producto por id

[POST] /api/productos - Guarda un producto en la base

[PUT] /api/productos/:id - Modifica un producto por id

[DELETE] /api/productos/:id - Elimina un producto por id

[GET] /info - Muestra información del global process

[GET] /random?cant=99999 - calculo un numero random de 1 a 1000 por la cant de veces recibida

[POST] /forceShutdown - apago el server y muestro por consola el exit code

## UI

/ingreso (autenticado unicamente)

/login

/register

## Inicio del server

npm start [CLUSTER] [PORT] [FB_CLIENT_ID] [FB_CLIENT_SECRET]

(\*) CLUSTER, si se envía el string CLUSTER este va a iniciar el server en modo cluster, sinó será creado tipo fork
